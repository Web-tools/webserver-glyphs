# Webserver Glyphs

Supplemental glyphs that can be used with _Apache's AutoIndxer_ module or the php *AutoIndex Site Browser*.

## Large Glyphs
<img src="/icons64/home.64.png" />
<img src="/icons64/home2.64.png" />
<img src="/icons64/slash.64.png" />
<img src="/icons64/slash2.64.png" />
<img src="/icons64/compress-red.64.png" />
<img src="/icons64/compress-blue.64.png" />
<img src="/icons64/compress-green.64.png" />
<img src="/icons64/compress-yellow.64.png" />

## Small Glyphs
<img src="/icons/home.png" />
<img src="/icons/home2.png" />
<img src="/icons/diskiso.png" />
<img src="/icons/compress-red.png" />
<img src="/icons/compress-blue.png" />
<img src="/icons/compress-green.png" />
<img src="/icons/compress-yellow.png" />
